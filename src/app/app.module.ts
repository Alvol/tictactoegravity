import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {HeaderComponent} from './header/header.component';
import {GameComponent} from './game/game.component';
import {RouterModule, Routes} from '@angular/router';
import {UserService} from './user.service';
import {GameService} from './game.service';

// const appRoutes: Routes = [
//
// ];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    GameComponent,
  ],
  imports: [
    RouterModule.forRoot(
      [{ path: 'home', component: HeaderComponent },
      { path: 'game', component: GameComponent }],
      { enableTracing: false} // <-- debugging purposes only
    ),
    BrowserModule
  ],
  providers: [UserService, GameService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
