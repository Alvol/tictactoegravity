import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {GameService} from '../game.service';
import {Cell} from '../models/cell.model';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css']
})
export class GameComponent implements OnInit {

  boardSize = 5;
  board: any[] = [];
  @ViewChild("gameBoard", {read: ElementRef}) gameBoard: ElementRef;

  constructor(private gameService: GameService) {
  }

  ngOnInit() {
    this.board = this.gameService.generateBoard(this.boardSize);
  }

  checkCellValue(button: Cell) {
    console.log(button.id, button.value);
    this.board[button.x].forEach((cell) => {
      console.log('check column ', cell.id);
    });
  }

  onGridButtonClick(cell: Cell) {
    console.log('onGridButtonClick', cell.x);
    this.checkCellValue(cell);
  }

}
