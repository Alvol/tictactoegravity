import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.tpl.html'
})
export class HeaderComponent {

  @Input()userName: string;

  constructor() {
  }

  onStartClick(userName) {
    this.userName = userName;
    console.log('start click', this.userName );
  }
}
