import { Injectable } from '@angular/core';
import {Cell} from './models/cell.model';

@Injectable()
export class GameService {

  constructor() { }

  generateBoard(boardSize): any[] {
    let board: any[] = [];
    for (let y = 0; y < boardSize; y++) {
      let row: Cell[] = [];
      for (let x = 0; x < boardSize; x++) {
        let gridCell: Cell = {
          id: 'i' + x + y,
          y: y, x: x,
          value: ''};
        row.push(gridCell);
      }
      board.push(row);
    }
    return board;
  }

}
