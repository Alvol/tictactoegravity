export interface Cell {
  id: string;
  y: number;
  x: number;
  value: string;

}
