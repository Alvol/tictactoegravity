const HttpError = require('../error').HttpError;

module.exports = function(req, res, next) {
  // мидлвер на авторизацию, это моя таска на 2м проекте
  if (!req.session.user) {
    return next(new HttpError(401, "Вы не авторизованы"));
  }

  next();
};
