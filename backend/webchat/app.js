const express = require('express');
const path = require('path');

const config = require('./config'); // я как трушный девелопер конфиги в файл вынес
const session = require('express-session'); // откуда ворую и куда ставлю инфу о сессии
const sessionStore = require('./libs/sessionStore');//недоработано будет матюкаться заработает и без него

const favicon = require('serve-favicon'); // фавиконка, пока не подумал что на нее поставить
const logger = require('morgan'); // логер, нихера не работает но зоть шаблон под него, раньше был винстон но я его случайно убил и  не смог востановить
const cookieParser = require('cookie-parser'); // эта хрень достает куки с запроса
const bodyParser = require('body-parser'); //парсит это куки
const errorhandler = require('errorhandler') // для переопределения ошибок, забей на это
// вьюшки
const index = require('./routes/index');
const users = require('./routes/users');
const login = require('./routes/login');
const logout = require('./routes/logout');
const chat = require('./routes/chat');

const HttpError = require('./error').HttpError; // кастомная обработка ошибок, как обвалиться поймешь почему так

const app = express();

// view engine setup
app.engine('ejs', require('ejs-locals')); // сечу аозможность читать еджс
app.set('views', path.join(__dirname, 'template')); // подключаю вьюхи
app.set('view engine', 'ejs'); // возмодность читать еджс

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
// парсеры всего и вся, не трогай
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(session({
  secret: config.get('session:secret'), // чтот типа нахер надо хз но в примере гуры было имеет вид типа ABCDE242342342314123421.SHA256
  resave: false, // хз шо за поле но так у гуры
  saveUninitialized: true, // хз шо за поле но так у гуры
  key: config.get('session:key'), //  хз че но пока не материлось
  cookie: config.get('session:cookie'), // кука
  store: sessionStore // пока не доделано, если будет падать - убирай
}));

app.use(express.static(path.join(__dirname, 'public'))); // даю доступ внешнему юзеру к всему что в папке паблик
app.use(require('./middleware/sendHttpError'));
app.use(require('./middleware/loadUser'));

app.use('/', index);
app.use('/users', users);
app.use('/login', login);
app.use('/logout', logout);
app.use('/chat', chat);

//красивая эрор 404
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// человеческая обработка ошибки, ну типа когда-то на прод это выложу
app.use(function(err, req, res, next) {
  if (typeof err == 'number') {
    err = new HttpError(err);
  }

  if (err instanceof HttpError) {
    res.sendHttpError(err);
  } else {
    if (app.get('env') == 'development') {
      errorhandler()(err, req, res, next);
    } else {

      err = new HttpError(500);
      res.sendHttpError(err);
    }
  }
});
module.exports = app;
