var express = require('express');
var router = express.Router();

// начальная страница нигде не юзается, у меня она другая, эту стандартно создала нода
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

module.exports = router;
