const express = require('express');
const router = express.Router();
const checkAuth = require('../middleware/checkAuth');
//сначало это
router.use('/', checkAuth);
//потом это
router.get('/', function(req, res) {
  res.render('chat');
});

module.exports = router;
