const express = require('express');
const router = express.Router();
const User = require('../models/user').User;

router.get('/', (req, res) => {
  res.render('login');
});
//посмотри на то что отправляет форма в логин темплейте, поймешь что к чему
router.post('/', (req, res, next) => {
    // req -откуда читаем
    // res - куда пишем
    // что делаем дальшеб обычно делаеться проверка на изнекстб но у меня это обработчик ошибок и он всегда есть
  const username = req.body.username;
  const password = req.body.password;

  User.authorize(username, password)
    .then((user) => {
      req.session.user = user._id;
      res.send({});
    })
    .catch((err) => {
      console.log(err);
      next(err);
    });
});

module.exports = router;
