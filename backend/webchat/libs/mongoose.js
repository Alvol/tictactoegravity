var mongoose = require('mongoose');
var config = require('../config');
//общий конфиг мангуса, смотри в модельки там оно применяется. По идее часто повторяющийся кусок
mongoose.connect(config.get('mongoose:uri'), config.get('mongoose:options'));

module.exports = mongoose;
